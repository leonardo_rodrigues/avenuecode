O projeto foi feito com maven, usando o spring boot, bem como o arquétipo do mesmo. O projeto por padrão herdou os comandos de inicialização do spring boot.

Os comandos são:

Inicialização: mvn spring-boot:run

Teste: mvn test

URLs:

http://<ip da maquina com a aplicação>:8080/script

http://<ip da maquina com a aplicação>:8080/settings

http://<ip da maquina com a aplicação>:8080/settings/<id do setting>

http://<ip da maquina com a aplicação>:8080/characters

http://<ip da maquina com a aplicação>:8080/characters/<id do character>

The project has been made in maven, using spring boot, so do his archetype. The project inherited the initialization commands from spring boot by default.

The commands are:

Initialization: mvn spring-boot:run

Test: mvn test

URLs:

http://<machine's ip where the application is running>:8080/script

http://<machine's ip where the application is running>:8080/settings

http://<machine's ip where the application is running>:8080/settings/<id of setting>

http://<machine's ip where the application is running>:8080/characters

http://<machine's ip where the application is running>:8080/characters/<id of character>