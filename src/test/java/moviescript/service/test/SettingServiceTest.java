package moviescript.service.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.ac.moviescript.model.Setting;
import br.com.ac.moviescript.service.ScriptService;
import br.com.ac.moviescript.service.SettingService;

import com.google.common.collect.Lists;

public class SettingServiceTest extends ServiceTest {
	
	@Autowired
	private SettingService settingService;
	
	@Autowired
	private ScriptService scriptService;
	
	@Test
	public void mustHaveTwoSettings() {
		// [FIXME] Maybe some bug with eclipse, when the test goes by his environment, 
		// this test happens first, when by maven command this test happens at the end
		//scriptService.save(movieScriptForSetting);
		List<Setting> settings = Lists.newArrayList(settingService.findAllSettings());
		Assert.assertNotNull(settings);
		Assert.assertFalse(settings.isEmpty());
		Assert.assertEquals(2, settings.size());
	}
}
