package moviescript.service.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.ac.moviescript.exception.ScriptAlreadyExistsException;
import br.com.ac.moviescript.service.ScriptService;

public class ScriptServiceTest extends ServiceTest {
	
	@Autowired
	private ScriptService scriptService;
	
	@Test
	public void IfScriptNotExistsSaveAndExistsNormally() {
		scriptService.save(movieScriptForScript);
	}

	@Test(expected = ScriptAlreadyExistsException.class)
	public void IfScriptExistsThrowException() {
		scriptService.save(movieScriptForScript);
		scriptService.save(movieScriptForScript);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void IfScriptIsEmptyThrowException() {
		scriptService.save("");
	}
}
