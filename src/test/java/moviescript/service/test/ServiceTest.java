package moviescript.service.test;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.ac.moviescript.ApplicationServer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ApplicationServer.class)
public abstract class ServiceTest {
	
	protected static String movieScriptForScript;	
	protected static String movieScriptForSetting;
	protected static String settingForCharacter;
	protected static String speechForCharacter;
	
	@BeforeClass
	public static void setUp() {
		movieScriptForScript = new StringBuilder("INT./EXT. LUKE'S SPEEDERS - DESERT WASTELAND - TRAVELING - DAY\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir.\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n")
		.append("INT. LUKE'S XWING FIGHTER - COCKPIT - TRAVELING")
		.append("                     THREEPIO\n")
		.append("          Artoo-Detoo! It's you! It's you!\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n").toString();
		
		movieScriptForSetting = new StringBuilder("INT./EXT. LUKE'S SPEEDER - DESERT WASTELAND - TRAVELING - DAY\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir.\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n")
		.append("INT. LUKE'S XWING FIGHTERS - COCKPIT - TRAVELING")
		.append("                     THREEPIO\n")
		.append("          Artoo-Detoo! It's you! It's you!\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n").toString();
		
		settingForCharacter = "INT. LUKE'S XWING FIGHTERS - COCKPIT - TRAVELING";
		
		speechForCharacter = new StringBuilder()
		.append("                     THREEPIO\n")
		.append("          Artoo-Detoo! It's you! It's you!\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n").toString();
	}
	
}
