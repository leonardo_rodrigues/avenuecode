package moviescript.extractor.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.ac.moviescript.extractor.impl.CharacterExtractor;
import br.com.ac.moviescript.model.Character;

public class CharacterExtractorTest {
	
	@Test
	public void extractOneSpeech() {
		StringBuilder characterSpeech = new StringBuilder("                      THREEPIO\n")
		.append("          Artoo-Detoo! It's you! It's you!");
		CharacterExtractor extractor = new CharacterExtractor();
		extractor.setTextToExtract(characterSpeech);
		List<Character> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(1, results.size());
		Assert.assertTrue(results.stream().anyMatch(c -> c.getNmCharacter().equals("THREEPIO")));
	}
	
	@Test
	public void extractMultipleSpeech() {
		StringBuilder charactersSpeech = new StringBuilder("                      THREEPIO\n")
		.append("          I see, sir.\n")
		.append("                      LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                      THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                      LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n");
		CharacterExtractor extractor = new CharacterExtractor();
		extractor.setTextToExtract(charactersSpeech);
		List<Character> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(2, results.size());
		Assert.assertTrue(results.stream().anyMatch(c -> c.getNmCharacter().equals("THREEPIO")));
		Assert.assertTrue(results.stream().anyMatch(c -> c.getNmCharacter().equals("LUKE")));
	}
	
	@Test
	public void extractCharacterAlongWithTheSpeech() {
		StringBuilder movieScript = new StringBuilder()
		.append("                      THREEPIO\n")
		.append("          I see, sir.\n")
		.append("                      LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                      THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                      LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n");

		CharacterExtractor extractor = new CharacterExtractor();
		extractor.setTextToExtract(movieScript);
		List<Character> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(2, results.size());
		Assert.assertNotNull(results.get(0).getDeDialog());
		Assert.assertNotNull(results.get(1).getDeDialog());
		
		Character threepio = results.stream().filter(c -> c.getNmCharacter().equals("THREEPIO")).findFirst().get();
		Character luke = results.stream().filter(c -> c.getNmCharacter().equals("LUKE")).findFirst().get();

		Assert.assertFalse(threepio.getDeDialog().indexOf("THREEPIO") > -1);
		Assert.assertFalse(luke.getDeDialog().indexOf("LUKE") > -1);
		Assert.assertTrue(threepio.getDeDialog().indexOf("I see, sir") > -1);
		Assert.assertTrue(luke.getDeDialog().indexOf("Uh, you can call me Luke") > -1);
		Assert.assertTrue(threepio.getDeDialog().indexOf(" I see, sir Luke") > -1);
		Assert.assertTrue(luke.getDeDialog().indexOf("Just Luke") > -1);
		Assert.assertTrue(threepio.getNmCharacter().equals("THREEPIO"));
		Assert.assertTrue(luke.getNmCharacter().equals("LUKE"));
	}
}
