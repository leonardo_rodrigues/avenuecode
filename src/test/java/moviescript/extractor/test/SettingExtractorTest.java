package moviescript.extractor.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.ac.moviescript.extractor.impl.SettingExtractor;
import br.com.ac.moviescript.model.Setting;

public class SettingExtractorTest {

	@Test
	public void extractOneSetting() {
		SettingExtractor extractor = new SettingExtractor();
		extractor.setTextToExtract(new StringBuilder("INT./EXT. LUKE'S SPEEDERS - DESERT WASTELAND - TRAVELING DAY"));
		List<Setting> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(1, results.size());
		Assert.assertEquals(results.get(0).getNmSetting(), "LUKE'S SPEEDERS");
	}
	
	@Test
	public void extractMultipleSettings() {
		StringBuilder movieScript = new StringBuilder("INT./EXT. LUKE'S SPEEDERS - DESERT WASTELAND - TRAVELING - DAY\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir.\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n")
		.append("INT. LUKE'S XWING FIGHTER - COCKPIT - TRAVELING");

		SettingExtractor extractor = new SettingExtractor();
		extractor.setTextToExtract(movieScript);
		List<Setting> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(2, results.size());
		Assert.assertTrue(results.stream().anyMatch(s -> s.getNmSetting().equals("LUKE'S SPEEDERS")));
		Assert.assertTrue(results.stream().anyMatch(s -> s.getNmSetting().equals("LUKE'S XWING FIGHTER")));
	}
	
	@Test
	public void extractSettingAlongWithTheDialog() {
		StringBuilder movieScript = new StringBuilder("INT./EXT. LUKE'S SPEEDERS - DESERT WASTELAND - TRAVELING - DAY\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir.\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n")
		.append("INT. LUKE'S XWING FIGHTER - COCKPIT - TRAVELING\n")
		.append("                     THREEPIO\n")
		.append("          Artoo-Detoo! It's you! It's you!\n")
		.append("                     LUKE\n")
		.append("          Uh, you can call me Luke.\n")
		.append("                     LUKE\n")
		.append("               (laughing)\n")
		.append("          Just Luke\n")
		.append("                     THREEPIO\n")
		.append("          I see, sir Luke.\n");

		SettingExtractor extractor = new SettingExtractor();
		extractor.setTextToExtract(movieScript);
		List<Setting> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(2, results.size());
		Assert.assertNotNull(results.get(0).getDeSetting());
		Assert.assertNotNull(results.get(1).getDeSetting());
		Assert.assertTrue(results.stream().anyMatch(s -> s.getNmSetting().equals("LUKE'S SPEEDERS")));
		Assert.assertTrue(results.stream().anyMatch(s -> s.getNmSetting().equals("LUKE'S XWING FIGHTER")));
	}
}
