package moviescript.extractor.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.ac.moviescript.extractor.impl.WordExtractor;
import br.com.ac.moviescript.model.Word;

public class WordExtractorTest {

	@Test
	public void extractOneSpeechWithWordCount() {
		StringBuilder speech = new StringBuilder("          Artoo-Detoo! It's you! It's you!");
		WordExtractor extractor = new WordExtractor();
		extractor.setTextToExtract(speech);
		List<Word> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(4, results.size());
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("artoo") && w.getNuCount() == 1));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("detoo") && w.getNuCount() == 1));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("it's") && w.getNuCount() == 2));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("you") && w.getNuCount() == 2));
	}
	
	@Test
	public void extractMultipleSpeechWithWordCount() {
		StringBuilder speech = new StringBuilder()
		.append("          Artoo-Detoo! It's you! It's you!\n")
		.append("          I see, sir.\n")
		.append("          I see, sir Luke.\n");
		WordExtractor extractor = new WordExtractor();
		extractor.setTextToExtract(speech);
		List<Word> results = extractor.extract();
		Assert.assertFalse(results.isEmpty());
		Assert.assertEquals(8, results.size());
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("artoo") && w.getNuCount() == 1));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("detoo") && w.getNuCount() == 1));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("it's") && w.getNuCount() == 2));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("you") && w.getNuCount() == 2));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("i") && w.getNuCount() == 2));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("see") && w.getNuCount() == 2));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("sir") && w.getNuCount() == 2));
		Assert.assertTrue(results.stream().anyMatch(w -> w.getDeWord().equals("luke") && w.getNuCount() == 1));
	}
}
