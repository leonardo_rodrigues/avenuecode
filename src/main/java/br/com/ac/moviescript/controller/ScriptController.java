package br.com.ac.moviescript.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ac.moviescript.message.MessageBundle;
import br.com.ac.moviescript.service.ScriptService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
public class ScriptController {
	
	@Autowired
	private ScriptService scriptService;

	private static final Logger LOG = LoggerFactory.getLogger(ScriptController.class);
	
	@RequestMapping(value="/script", method = RequestMethod.POST, consumes = {MediaType.TEXT_PLAIN_VALUE})
	public ResponseEntity<?> save(@RequestBody String deScript) {
		try {
			scriptService.save(deScript);
		} catch(RuntimeException ex) {
			LOG.warn(ex.getMessage(), ex);
			ObjectNode node = new ObjectMapper().createObjectNode();
			node.put("message", ex.getMessage());
			return new ResponseEntity<>(node, HttpStatus.FORBIDDEN); 
		} catch(Exception ex) {
			LOG.error(ex.getMessage(), ex);
			return new ResponseEntity<>(new MessageBundle().getMessage("default.msg.error.unexpected"), HttpStatus.INTERNAL_SERVER_ERROR); 
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
