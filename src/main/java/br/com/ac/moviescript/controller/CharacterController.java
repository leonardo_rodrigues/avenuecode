package br.com.ac.moviescript.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ac.moviescript.message.MessageBundle;
import br.com.ac.moviescript.service.CharacterService;

@RestController
@RequestMapping(value = "/characters")
public class CharacterController {
	
	@Autowired
	private CharacterService characterService;

	private static final Logger LOG = LoggerFactory.getLogger(CharacterController.class);
	
	@RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> findAllCharacters() {
		try {
			return new ResponseEntity<>(characterService.findAllCharacters(), HttpStatus.OK);
		} catch(Exception ex) {
			LOG.error(ex.getMessage(), ex);
			return new ResponseEntity<>(new MessageBundle().getMessage("default.msg.error.unexpected"), HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}
	
	@RequestMapping(value="/{cdCharacter}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> findCharacterByCdCharacter(@PathVariable("cdCharacter") Long cdCharacter) {
		try {
			return new ResponseEntity<>(characterService.findCharacterByCdCharacter(cdCharacter), HttpStatus.OK);
		} catch(Exception ex) {
			LOG.error(ex.getMessage(), ex);
			return new ResponseEntity<>(new MessageBundle().getMessage("default.msg.error.unexpected"), HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}
}
