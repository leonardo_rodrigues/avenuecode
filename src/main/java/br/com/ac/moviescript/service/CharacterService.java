package br.com.ac.moviescript.service;

import br.com.ac.moviescript.model.Character;
import br.com.ac.moviescript.model.Setting;

public interface CharacterService {
	
	/**
	 * Extract the character and his dialogs from setting, 
	 * and then save.
	 * 
	 * @param Setting setting
	 */
	void extractDialogsFromSettingAndSave(Setting setting);
	Character findCharacterByCdCharacter(Long cdCharacter);
	Iterable<Character> findAllCharactersByCdSetting(Long cdSetting);
	Iterable<Character> findAllCharacters();
}
