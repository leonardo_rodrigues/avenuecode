package br.com.ac.moviescript.service;

import br.com.ac.moviescript.model.Character;
import br.com.ac.moviescript.model.Word;

public interface WordService {
	/**
	 * Extract the word and count from character dialog, 
	 * and then save.
	 * 
	 * @param Character character
	 */
	void extractWordsFromCharacterDialogAndSave(Character character);
	Iterable<Word> findTenthMostSpokenWordsByCdCharacter(Long cdCharacter);
}
