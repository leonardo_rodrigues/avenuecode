package br.com.ac.moviescript.service;

import br.com.ac.moviescript.model.Script;
import br.com.ac.moviescript.model.Setting;

public interface SettingService {
	/**
	 * Extract the settings from the script and then save.
	 * 
	 * @param <code>Script</code>
	 * 
	 */
	void extractSettingFromScriptAndSave(Script script);
	Setting findSettingByCdSetting(Long cdSetting);
	Iterable<Setting> findAllSettings();
}
