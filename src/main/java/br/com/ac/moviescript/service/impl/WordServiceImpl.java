package br.com.ac.moviescript.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.ac.moviescript.extractor.impl.WordExtractor;
import br.com.ac.moviescript.model.Character;
import br.com.ac.moviescript.model.Word;
import br.com.ac.moviescript.repository.WordRepository;
import br.com.ac.moviescript.service.WordService;

@Service
public class WordServiceImpl implements WordService {

	@Autowired
	private WordRepository wordRepository;
	
	@Transactional
	@Override
	public void extractWordsFromCharacterDialogAndSave(Character character) {
		WordExtractor extractor = new WordExtractor();
		extractor.setTextToExtract(new StringBuilder(character.getDeDialog()));
		List<Word> words = extractor.extract();
		words.stream().forEach((w) -> {
			w.setCharacter(character);
			wordRepository.save(w);
		});
	}

	@Override
	public Iterable<Word> findTenthMostSpokenWordsByCdCharacter(Long cdCharacter) {
		PageRequest page = new PageRequest(0, 10, Direction.DESC, "nuCount");
		return wordRepository.findAllWordsPageableByCdCharacter(cdCharacter, page);
	}

}
