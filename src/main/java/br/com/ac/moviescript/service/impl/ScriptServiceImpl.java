package br.com.ac.moviescript.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ac.moviescript.exception.ScriptAlreadyExistsException;
import br.com.ac.moviescript.message.MessageBundle;
import br.com.ac.moviescript.model.Script;
import br.com.ac.moviescript.repository.ScriptRepository;
import br.com.ac.moviescript.service.ScriptService;
import br.com.ac.moviescript.service.SettingService;

@Service
public class ScriptServiceImpl implements ScriptService {

	@Autowired
	private ScriptRepository scriptRepository;
	
	@Autowired
	private SettingService settingService;
	
	@Transactional
	@Override
	public void save(String deScript) {
		Script script = new Script();
		script.setDeScript(deScript);
		if(script.getDeScript() == null || script.getDeScript().isEmpty()) {
			throw new IllegalArgumentException(new MessageBundle().getMessage("default.msg.error.unexpected"));
		}
		if(isDeScriptAlreadySaved(script.getDeScript())) {
			throw new ScriptAlreadyExistsException();
		}
		scriptRepository.save(script);
		settingService.extractSettingFromScriptAndSave(script);
	}
	
	private boolean isDeScriptAlreadySaved(String deScript) {
		return scriptRepository.findByDeScript(deScript) != null;
	}
}
