package br.com.ac.moviescript.service.impl;

import java.util.List;




import javax.transaction.Transactional;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import com.google.common.collect.Lists;

import br.com.ac.moviescript.extractor.impl.CharacterExtractor;
import br.com.ac.moviescript.model.Character;
import br.com.ac.moviescript.model.Setting;
import br.com.ac.moviescript.repository.CharacterRepository;
import br.com.ac.moviescript.service.CharacterService;
import br.com.ac.moviescript.service.WordService;


@Service
public class CharacterServiceImpl implements CharacterService {

	@Autowired
	private CharacterRepository characterRepository;
	
	@Autowired
	private WordService wordService;
	
	public Iterable<Character> findAllCharacters() {
		Iterable<Character> characters = characterRepository.findAll();
		if(characters == null) {
			return null;
		}
		characters.forEach((c) -> {
			c.setWordCounts(Lists.newArrayList(wordService.findTenthMostSpokenWordsByCdCharacter(c.getCdCharacter())));
		});
		return characters;
	}
	
	@Override
	public Iterable<Character> findAllCharactersByCdSetting(Long cdSetting) {
		Iterable<Character> characters = characterRepository.findAllCharactersByCdSetting(cdSetting);
		if(characters == null) {
			return null;
		}
		characters.forEach((c) -> {
			c.setWordCounts(Lists.newArrayList(wordService.findTenthMostSpokenWordsByCdCharacter(c.getCdCharacter())));
		});
		return characters;
	}
	
	@Override
	public Character findCharacterByCdCharacter(Long cdCharacter) {
		Character character = characterRepository.findCharacterByCdCharacter(cdCharacter);
		if(character == null) {
			return null;
		}
		character.setWordCounts(Lists.newArrayList(wordService.findTenthMostSpokenWordsByCdCharacter(cdCharacter)));
		return character;
	}

	@Transactional
	@Override
	public void extractDialogsFromSettingAndSave(Setting setting) {
		CharacterExtractor extractor = new CharacterExtractor();
		extractor.setTextToExtract(new StringBuilder(setting.getDeSetting()));
		List<Character> characters = extractor.extract();
		characters.stream().forEach((c) -> {
			c.setSetting(setting);
			characterRepository.save(c);
			wordService.extractWordsFromCharacterDialogAndSave(c);
		});
	}

}
