package br.com.ac.moviescript.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import br.com.ac.moviescript.extractor.impl.SettingExtractor;
import br.com.ac.moviescript.model.Script;
import br.com.ac.moviescript.model.Setting;
import br.com.ac.moviescript.repository.SettingRepository;
import br.com.ac.moviescript.service.CharacterService;
import br.com.ac.moviescript.service.SettingService;

@Service
public class SettingServiceImpl implements SettingService {

	@Autowired
	private SettingRepository settingRepository;
	
	@Autowired
	private CharacterService characterService;
	
	@Transactional
	@Override
	public void extractSettingFromScriptAndSave(Script script) {
		SettingExtractor extractor = new SettingExtractor();
		extractor.setTextToExtract(new StringBuilder(script.getDeScript()));
		List<Setting> settings = extractor.extract();
		settings.stream().forEach((s) -> {
			s.setScript(script);
			settingRepository.save(s);
			characterService.extractDialogsFromSettingAndSave(s);
		});
	}

	@Override
	public Setting findSettingByCdSetting(Long cdSetting) {
		Setting setting = settingRepository.findSettingByCdSetting(cdSetting);
		if(setting == null) {
			return null;
		}
		setting.setCharacters(Lists.newArrayList(characterService.findAllCharactersByCdSetting(cdSetting)));
		return setting;
	}
	
	@Override
	public Iterable<Setting> findAllSettings() {
		Iterable<Setting> settings = settingRepository.findAll();
		if(settings == null) {
			return null;
		}
		settings.forEach((s) -> { 
			s.setCharacters(Lists.newArrayList(characterService.findAllCharactersByCdSetting(s.getCdSetting())));
		});
		return settings;
	}

}
