package br.com.ac.moviescript.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.ac.moviescript.model.Script;

@Repository
public interface ScriptRepository extends CrudRepository<Script, Long> {
	
	@Query("select s from Script s where s.deScript = ?1")
	Script findByDeScript(String deScript);

}
