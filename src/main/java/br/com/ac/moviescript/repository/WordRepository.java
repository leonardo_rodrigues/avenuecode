package br.com.ac.moviescript.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.ac.moviescript.model.Word;

@Repository
public interface WordRepository extends PagingAndSortingRepository<Word, Long> {
	@Query("select w from Word w join w.character c where c.cdCharacter = ?1")
	Page<Word> findAllWordsPageableByCdCharacter(Long cdCharacter, Pageable pageable);
}
