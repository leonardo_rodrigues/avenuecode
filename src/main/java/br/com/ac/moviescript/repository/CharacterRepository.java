package br.com.ac.moviescript.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.ac.moviescript.model.Character;

@Repository
public interface CharacterRepository extends CrudRepository<Character, Long>{
	
	@Query("select c from Character c join c.setting s where s.cdSetting = ?1")
	Iterable<Character> findAllCharactersByCdSetting(Long cdSetting);
	
	@Query("select c from Character c where c.cdCharacter = ?1")
	Character findCharacterByCdCharacter(Long cdCharacter);
	
}
