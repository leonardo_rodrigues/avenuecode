package br.com.ac.moviescript.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.ac.moviescript.model.Setting;

@Repository
public interface SettingRepository extends CrudRepository<Setting, Long> {
	
	@Query("select s from Setting s where s.cdSetting = ?1")
	Setting findSettingByCdSetting(Long cdSetting);
	
}
