package br.com.ac.moviescript.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Script implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5044200142568715346L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cdScript")
	private Long cdScript;
	
	@Column(name = "deScript", length = Integer.MAX_VALUE)
	private String deScript;
	
	public Script() {}
	
	public Long getCdScript() {
		return cdScript;
	}

	public void setCdScript(Long cdScript) {
		this.cdScript = cdScript;
	}

	public String getDeScript() {
		return deScript;
	}

	public void setDeScript(String deScript) {
		this.deScript = deScript;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(cdScript).append(deScript).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Script)) {
			return false;
		}
		return new EqualsBuilder()
		.append(this.cdScript, ((Script)obj).cdScript)
		.append(this.deScript, ((Script)obj).deScript).isEquals();
	}
	
}
