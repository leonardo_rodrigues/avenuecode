package br.com.ac.moviescript.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.ac.moviescript.extractor.Extractable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Character implements Serializable, Extractable {
	
	public static final String SPEECH_SEPARATOR = "|";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2130664725889868065L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cdCharacter")
	@JsonProperty("id")
	private Long cdCharacter;

	@JsonProperty("name")
	@Column(name = "nmCharacter")
	private String nmCharacter;
	
	@JsonIgnore
	private transient String deDialog;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "cdSetting")
	private Setting setting;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "character")
	private List<Word> wordCounts;
	
	public Character() {
		wordCounts = new ArrayList<>();
	}

	public Long getCdCharacter() {
		return cdCharacter;
	}

	public void setCdCharacter(Long cdCharacter) {
		this.cdCharacter = cdCharacter;
	}

	public String getNmCharacter() {
		return nmCharacter;
	}

	public void setNmCharacter(String nmCharacter) {
		this.nmCharacter = nmCharacter;
	}

	public String getDeDialog() {
		return deDialog;
	}

	public void setDeDialog(String deDialog) {
		this.deDialog = deDialog;
	}

	public List<Word> getWordCounts() {
		return wordCounts;
	}

	public void setWordCounts(List<Word> wordCounts) {
		this.wordCounts = wordCounts;
	}

	public Setting getSetting() {
		return setting;
	}

	public void setSetting(Setting setting) {
		this.setting = setting;
	}
	
	public void appendDeDialog(String deDialog) {
		this.deDialog += SPEECH_SEPARATOR + deDialog;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(cdCharacter).append(nmCharacter).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Character)) {
			return false;
		}
		return new EqualsBuilder()
		.append(this.cdCharacter, ((Character)obj).cdCharacter)
		.append(this.nmCharacter, ((Character)obj).nmCharacter).isEquals();
	}
	
	@JsonIgnore
	@Override
	public String getNmExtraction() {
		return getNmCharacter();
	}
}
