package br.com.ac.moviescript.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.ac.moviescript.model.Character;

@Entity
public class Setting implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1523370205061129060L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cdSetting")
	@JsonProperty("id")
	private Long cdSetting;
	
	@JsonProperty("name")
	@Column(name = "nmCharacter")
	private String nmSetting;
	
	@JsonIgnore
	private transient String deSetting;
	
	@JsonIgnore
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "cdScript")
	private Script script;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "setting")
	private List<Character> characters;

	public Setting() {}
	
	public Long getCdSetting() {
		return cdSetting;
	}

	public void setCdSetting(Long cdSetting) {
		this.cdSetting = cdSetting;
	}

	public String getNmSetting() {
		return nmSetting;
	}

	public void setNmSetting(String nmSetting) {
		this.nmSetting = nmSetting;
	}

	public String getDeSetting() {
		return deSetting;
	}

	public void setDeSetting(String deSetting) {
		this.deSetting = deSetting;
	}

	public Script getScript() {
		return script;
	}

	public void setScript(Script script) {
		this.script = script;
	}

	public List<Character> getCharacters() {
		return characters;
	}

	public void setCharacters(List<Character> characters) {
		this.characters = characters;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(cdSetting).append(nmSetting).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Setting)) {
			return false;
		}
		return new EqualsBuilder()
		.append(this.cdSetting, ((Setting)obj).cdSetting)
		.append(this.nmSetting, ((Setting)obj).nmSetting).isEquals();
	}
}
