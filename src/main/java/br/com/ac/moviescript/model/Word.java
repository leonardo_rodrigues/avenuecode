package br.com.ac.moviescript.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.ac.moviescript.extractor.Extractable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Word implements Serializable, Extractable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1439088431142229890L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cdWord")
	@JsonProperty("id")
	private Long cdWord;

	@Column(name = "deWord")
	@JsonProperty("word")
	private String deWord;
	
	@Column(name = "nuCount")
	@JsonProperty("count")
	private Long nuCount;
	
	@JsonIgnore
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "cdCharacter")
	private Character character;

	public Word() {
		nuCount = 1L;
	}
	
	public Long getCdWord() {
		return cdWord;
	}

	public void setCdWord(Long cdWord) {
		this.cdWord = cdWord;
	}

	public String getDeWord() {
		return deWord;
	}

	public void setDeWord(String deWord) {
		this.deWord = deWord;
	}

	public Long getNuCount() {
		return nuCount;
	}

	public void setNuCount(Long nuCount) {
		this.nuCount = nuCount;
	}

	public Character getCharacter() {
		return character;
	}

	public void setCharacter(Character character) {
		this.character = character;
	}
	
	public void incrementNuCount() {
		nuCount++;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(cdWord).append(deWord).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Word)) {
			return false;
		}
		return new EqualsBuilder()
		.append(this.cdWord, ((Word)obj).cdWord)
		.append(this.deWord, ((Word)obj).deWord).isEquals();
	}
	
	@JsonIgnore
	@Override
	public String getNmExtraction() {
		return getDeWord();
	}
	
	@Override
	public String toString() {
		return getDeWord() + "=" + getNuCount();
	}
}
