package br.com.ac.moviescript.exception;

import br.com.ac.moviescript.message.MessageBundle;

public class ScriptAlreadyExistsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2097979807080022697L;

	public ScriptAlreadyExistsException() {
		super(new MessageBundle().getMessage("script.msg.warn.alreadyExists"));
	}
}
