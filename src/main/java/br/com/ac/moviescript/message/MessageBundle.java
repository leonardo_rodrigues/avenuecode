package br.com.ac.moviescript.message;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class MessageBundle {
	
	private static final String BUNDLE_DEFAULT_NAME = "ApplicationResources";
	
	private ResourceBundle bundle;
	
	public MessageBundle() {
		init(null);
	}
	
	public MessageBundle(Locale locale) {
		init(locale);
	}
	
	private void init(Locale locale) {
		if(locale == null) {
			bundle = ResourceBundle.getBundle(BUNDLE_DEFAULT_NAME);
			return;
		}
		bundle = ResourceBundle.getBundle(BUNDLE_DEFAULT_NAME, locale);
	}
	
	public String getMessage(String key) {
		return bundle.getString(key);
	}
	
	public String getMessage(String key, String... placeholders) {
		return MessageFormat.format(bundle.getString(key), (Object[])placeholders);
	}
}
