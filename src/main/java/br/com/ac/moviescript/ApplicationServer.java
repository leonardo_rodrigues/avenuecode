package br.com.ac.moviescript;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"br.com.ac.moviescript.controller", 
		"br.com.ac.moviescript.service", "br.com.ac.moviescript.repository"})
public class ApplicationServer {
	
	public static void main(String[] args) {
        SpringApplication.run(ApplicationServer.class, args);
    }
	
}
