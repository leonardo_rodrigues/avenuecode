package br.com.ac.moviescript.extractor.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.ac.moviescript.model.Character;

public class CharacterExtractor extends AbstractExtractor<Character> {
	
	public CharacterExtractor() {
		this.setRegexPattern("[\\s]{22}[A-Z\\s]*?(?:\\r\\n|[\\s]{15}|[\\s]{10})");
		this.setRegexPatternSplit("[\\s]{22}");
	}
	
	@Override
	protected List<Character> doExtraction() {
		Set<Character> characters = new HashSet<>();
		while(getMatcher().find()) {
			String nmCharacter = getMatcher().group().trim();
			Character character = generateCharFromScript(characters, nmCharacter);
			characters.add(character);
		}
		return new ArrayList<>(characters);
	}

	private Character generateCharFromScript(Set<Character> characters, String nmCharacter) {
		Character character = getExtractedEntity(nmCharacter, characters);
		if(character == null) {
			character = new Character();
			character.setNmCharacter(nmCharacter);
			character.setDeDialog(extractFromBody(nmCharacter));
		} else {
			character.appendDeDialog(extractFromBody(nmCharacter));
		}
		return character;
	}
	
}
