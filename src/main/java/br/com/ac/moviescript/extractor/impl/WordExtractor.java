package br.com.ac.moviescript.extractor.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.ac.moviescript.model.Word;

public class WordExtractor extends AbstractExtractor<Word> {
	
	public WordExtractor() {
		setRegexPattern("(?:[A-Z]|[a-z])[^\\!\\.\\?\\s\\-,]*");
		setRegexPatternSplit(" ");
	}
	
	@Override
	protected List<Word> doExtraction() {
		Set<Word> words = new HashSet<>();
		while(getMatcher().find()) {
			String deWord = getMatcher().group().toLowerCase();
			Word word = generateWordFromDialog(words, deWord);
			words.add(word);
		}
		return new ArrayList<>(words);
	}
	
	private Word generateWordFromDialog(Set<Word> words, String deWord) {
		Word word = getExtractedEntity(deWord, words);
		if(word == null) {
			word = new Word();
			word.setDeWord(deWord);
		} else {
			word.incrementNuCount();
		}
		return word;
	}
}
