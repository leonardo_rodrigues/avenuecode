package br.com.ac.moviescript.extractor;

import java.util.List;

public interface Extractor<T> {
	List<T> extract();
}
