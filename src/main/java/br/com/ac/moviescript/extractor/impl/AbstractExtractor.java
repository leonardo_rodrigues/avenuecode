package br.com.ac.moviescript.extractor.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.ac.moviescript.extractor.Extractable;
import br.com.ac.moviescript.extractor.Extractor;

abstract class AbstractExtractor<T> implements Extractor<T> {
	
	private StringBuilder textToExtract;
	private List<String> deBodies;
	private Pattern pattern;
	private Matcher matcher;
	private String regexPattern;
	private String regexPatternSplit;

	protected abstract List<T> doExtraction();
	
	protected void configureExtraction() {
		pattern = Pattern.compile(regexPattern);
		matcher = pattern.matcher(getTextToExtract());
	}
	
	public List<T> extract() {
		if(textToExtract == null) {
			return new ArrayList<>();
		}
		configureExtraction();
		return doExtraction();
	}
	
	protected String extractFromBody(String strSearch) {
		Iterator<String> bodyItr = getDeBodies().iterator();
		while(bodyItr.hasNext()) {
			String deBody = bodyItr.next();
			if(deBody.indexOf(strSearch) > -1) {
				bodyItr.remove();
				return deBody.replace(strSearch, "");
			}
		}
		return null;
	}
	
	protected static <E extends Extractable> E getExtractedEntity(String nmEntity, Set<E> entities) {
		for(E entity : entities) {
			if(entity.getNmExtraction().equals(nmEntity)) {
				return entity;
			}
		}
		return null;
	}

	public StringBuilder getTextToExtract() {
		return textToExtract;
	}

	public void setTextToExtract(StringBuilder textToExtract) {
		this.textToExtract = textToExtract;
	}

	private List<String> getDeBodies() {
		if(deBodies == null) {
			deBodies = new ArrayList<>();
			String[] deBodiesArr = textToExtract.toString().split(regexPatternSplit);
			for(String deBody : deBodiesArr) {
				deBodies.add(deBody);
			}
		}
		return deBodies;
	}
	
	protected Matcher getMatcher() {
		return matcher;
	}

	protected String getRegexPattern() {
		return regexPattern;
	}

	protected void setRegexPattern(String regexPattern) {
		this.regexPattern = regexPattern;
	}

	protected String getRegexPatternSplit() {
		return regexPatternSplit;
	}

	protected void setRegexPatternSplit(String regexPatternSplit) {
		this.regexPatternSplit = regexPatternSplit;
	}

}
