package br.com.ac.moviescript.extractor.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.ac.moviescript.model.Setting;

public class SettingExtractor extends AbstractExtractor<Setting> {
	
	public SettingExtractor() {
		setRegexPattern("(?:INT\\./EXT\\.\\s|INT\\.\\s|EXT\\.\\s).*?\\s-");
		setRegexPatternSplit("INT\\./EXT\\.\\s|INT\\.\\s|EXT\\.\\s");
	}

	@Override
	protected List<Setting> doExtraction() {
		Set<Setting> settings = new HashSet<>();
		while(getMatcher().find()) {
			String nmSetting = getMatcher().group().split(getRegexPatternSplit())[1].replace(" -", "");
			Setting setting = new Setting();
			setting.setNmSetting(nmSetting);
			setting.setDeSetting(extractFromBody(nmSetting));
			settings.add(setting);
		}
		return new ArrayList<>(settings);
	}

}
